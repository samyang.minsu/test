# test

SELECT  '블럭카운트 : ' + CAST(CNT AS VARCHAR(10)) FROM ( SELECT COUNT(*) CNT FROM SYSPROCESSES WHERE BLOCKED <> 0 ) AS A;
WITH    BLOCKING
AS (
        SELECT  
                SPID,
                BLOCKED,
                CAST(SPID AS VARCHAR(100))  AS BLOCKTREE,
                Level = 0 ,
                CAST(SPID AS VARBINARY(4000)) AS BLOCK_DEPTH
        FROM    MASTER.DBO.SYSPROCESSES
        WHERE   BLOCKED = 0 
                AND SPID > 50 
                AND SPID IN (SELECT BLOCKED 
					FROM MASTER.DBO.SYSPROCESSES WHERE BLOCKED <> 0)
               
        UNION ALL
               
        SELECT  DS.SPID ,
                DS.BLOCKED ,
                CAST(BC.BLOCKTREE + ' > ' 
			+ CAST(DS.SPID AS VARCHAR(100)) AS VARCHAR(100)) ,
                Level + 1 ,
                CAST(BLOCK_DEPTH + CAST(DS.BLOCKED AS BINARY(4)) AS VARBINARY(4000))
        FROM    BLOCKING AS BC
                INNER JOIN MASTER.DBO.SYSPROCESSES AS DS
                    ON DS.BLOCKED = BC.SPID
    )
SELECT
        BC.BLOCKTREE AS [블럭TREE],
        CONVERT(VARCHAR, DATEADD(S ,SP.waittime / 1000, ''), 8 ) AS [HH:MM:SS],
        DB_NAME(SP.dbid) AS [실행DB],
        SP.lastwaittype AS [대기유형],
        SP.waitresource AS [대기자원],
        SP.cmd AS [차단중인명령어],
        SP.hostname AS [호스트명],
        SP.program_name AS [실행프로그램],
        SP.loginame AS [실행계정],
        SQL_TEXT.TEXT AS [실행문]
        --SP.*
FROM    BLOCKING AS BC
        INNER JOIN MASTER.DBO.SYSPROCESSES AS SP
            ON BC.SPID = SP.SPID
        CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sql_text
ORDER BY BLOCK_DEPTH