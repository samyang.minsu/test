>> console.cloud.google.com

Google Cloud - GKE / GCP (방화벽설정 관련해서 모두 open 상태로 생성)
	kubernetes cluster - node 2개 / 4 core 15GB
	VM - 4 core 15GB / centos

>> VM 접속!
yum update -y
ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime && date
yum install -y wget vim yum-utils

>> VM - GKE 연동
sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
[google-cloud-sdk]
name=Google Cloud SDK
baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOM

yum install google-cloud-sdk -y
yum install kubectl -y



Kuberentes 클러스터 연결 화면 확인 : 클러스터에 연결 - 명령줄 액세스 입력
	gcloud container clusters get-credentials ~~~


// kubectl get node 하면 node 확인 가능!
kubectl get node


>> Docker 설치 (VM)
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum repolist

yum install docker-ce -y

systemctl start docker
systemctl enable docker



>> Jenkins 설치 - GKE cluster

//helm 설치 : URL : https://helm.sh/docs/intro/install/
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh

	?? 했는데 안되면 아래 명령어 해보기
	su - 


//Jenkins 설치
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm repo update

kubectl create namespace jenkins


// NodePort 로 변경하여 Jenkins install
helm install jenkins -n jenkins stable/jenkins --set master.serviceType=NodePort


// 서비스 포트 확인 가능
kubectl get svc -n jenkins

http://(노드 IP):port 로 접속
	>> 시간 조금 걸릴 수 있음!

## 만약 포트 접속이 안된다?
	GCP에서 네트워크 방화벽 설정 확인할 것!
	해당 포트 / 접속 ip 가 설정 안되어있을 가능성이 있음!

